public class Arithmetic{

  public static void main (String[] arg)
    {
      int sum = add (Integer.parseInt (arg[0]), Integer.parseInt (arg[1]));
      int diff = subtract (Integer.parseInt (arg[0]), Integer.parseInt (arg[1]));

        System.out.println ("Sum of numbers is " + sum);
        System.out.println ("Difference of numbers is " + diff);
    }

    public static int add (int i, int j)
    {
      return i + j;
    }

    public static int subtract (int i, int j)
    {
      return i - j;
    }

}
